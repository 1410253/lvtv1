from django.urls import path

from .views import ApartmentListView

urlpatterns = [
    path('allapartment',ApartmentListView.as_view()),
]
