from rest_framework.generics import ListAPIView,ListCreateAPIView
from public.models import Apartment
from .serializers import ApartmentSerializer,ApartmentSerializerForList


class ApartmentListView(ListAPIView):
    queryset = Apartment.objects.all()
    serializer_class = ApartmentSerializerForList
    