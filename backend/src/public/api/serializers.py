from rest_framework import serializers
from public.models import User,Apartment,ApartmentServices


#USER serializers
class UserSerializer(serializers.ModelSerializer):
    # profile = UserProfileSerializer(required=True)

    class Meta:
        model = User
        fields = '__all__'

class UserSerializerForList(serializers.ModelSerializer):
    # profile = UserProfileSerializer(required=True)
    class Meta:
        model = User
        fields = [
            'user_name',
            'email',
        ]


# APARTMENT serializers        

class ApartmentSerializer(serializers.ModelSerializer):
    # profile = UserProfileSerializer(required=True)

    class Meta:
        model = Apartment
        fields = '__all__'
class ApartmentSerializerForList(serializers.ModelSerializer):
    # profile = UserProfileSerializer(required=True)

    class Meta:
        model = Apartment
        fields = [
            'id',
            'avatar',
            'images',
            'title',
            'apartment_type',
            'price',
            'status',
            'updated_at'
        ]
class ApartmentServicesSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ApartmentServices
        fields = '__all__'
