from django.db import models
from decimal import Decimal
from django.contrib.postgres.fields import ArrayField
from django.utils import timezone


from django.core.validators import (
    FileExtensionValidator,
    MaxValueValidator,
    MinValueValidator,
)


# Create your models here.



zero = 0
one = 1
two = 2
three = 3

ROOMS = [
    (zero, 0),
    (one, 1),
    (two, 2),
    (three, 3)
]

STUDIO = 'studio'
CANHO = 'can_ho'
# XEMAY = 'xe_may'
# OTO = 'o_to'
# DULICH = 'du_lich'
# OTHER = 'other'

APARTMENT_TYPE = [
    (STUDIO, 'Studio'),
    (CANHO, 'Căn hộ'),
]

class City(models.Model):
    name = models.TextField()
    def __str__(self):
        return self.type

class District(models.Model):
    name = models.TextField()
    city_id = models.ForeignKey(
        City,
        to_field="id",
        db_column="city_id",
        on_delete=models.CASCADE,
        related_name='city_id',
    )
    def __str__(self):
        return self.type

class ApartmentLocation(models.Model):
    # apartment = models.ForeignKey(
    #     Apartment,
    #     to_field="id",
    #     db_column="apartment_id",
    #     related_name="apartment_location",
    #     on_delete=models.CASCADE,
    # )
    # V1: using textfield
    # city = models.CharField(
    #     max_length=50,
    #     choices=CITY,
    #     default=HCM,
    #     unique=True,
    # )
    # district = models.CharField(
    #     max_length=50,
    #     choices=DISTRICT,
    #     default=Q1,
    #     unique=True,
    # )
    city = models.TextField(unique=True)
    district = models.TextField(unique=True)
    address = models.TextField(unique=True)
    notes = models.TextField(unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)


class ApartmentType(models.Model):
    type = models.CharField(
        max_length=50,
        default=CANHO,
        unique=True,
    )
    # type = models.TextField(unique=True)
    description = models.TextField()

    def __str__(self):
        return self.type

class Apartment(models.Model):
    images = ArrayField(
        models.CharField(max_length=30, blank=True),
        size=8,
        default = list,
    )
    avatar = models.TextField(default="",max_length=100)
    title = models.CharField(max_length=100)
    apartment_type = models.ForeignKey(
        ApartmentType,
        to_field="id",
        db_column="apartment_type",
        on_delete=models.CASCADE,
        related_name='apartment_type',
    )
    price = models.DecimalField(
        max_digits=15,
        decimal_places=2,
        default=Decimal('0.00'),
        validators=[
            MinValueValidator(0.00),
            MaxValueValidator(999999999999)
        ],
    )
    status = models.SmallIntegerField(
        default = 0
    )
    # location = models.ForeignKey(
    #     ApartmentLocation,
    #     db_column="apartment_location",
    #     on_delete=models.CASCADE,
    #     related_name='apartment_location',
    # )


    #location 

    city_id = models.SmallIntegerField(
        default=1,
    )
    district_id = models.SmallIntegerField(
        default=1,
    )
    address = models.CharField(
        max_length=50,
        default = "",
    )


    # rooms 
    living_rooms = models.SmallIntegerField(
        choices = ROOMS,
        default = zero,
    )
    dinning_rooms = models.SmallIntegerField(
        choices=ROOMS,
        default=zero,
    )
    bed_rooms = models.SmallIntegerField(
        choices=ROOMS,
        default=zero,
    )
    bath_rooms = models.SmallIntegerField(
        choices=ROOMS,
        default=zero,
    )
    toilets = models.SmallIntegerField(
        choices=ROOMS,
        default=zero,
    )
    kitchen = models.SmallIntegerField(
        choices=ROOMS,
        default=zero,
    )

    description = models.TextField(default="", null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True , null = True)
    updated_at = models.DateTimeField(auto_now=True , null = True)

    def __str__(self):
        return self.title
class ApartmentServices (models.Model):
    title = models.CharField(max_length=100)
    apartment = models.ForeignKey(
        Apartment,
        to_field="id",
        db_column="apartment_id",
        on_delete=models.CASCADE,
        related_name='apartment_services',
    )
    cost = models.DecimalField(
        max_digits=15,
        decimal_places=2,
        default=Decimal('0.00'),
        validators=[
            MinValueValidator(0.00),
            MaxValueValidator(999999999999)
        ],
    )

    def __str__(self):
        return self.title
class User(models.Model):
        user_name = models.CharField(max_length=64)
        email = models.EmailField(max_length=255)
        first_name = models.TextField(null=True, blank=True)
        last_name = models.TextField(null=True, blank=True)
        active = models.BooleanField(default=True)
        created_at = models.DateTimeField(
        auto_now_add=True,
        )
        updated_at = models.DateTimeField(
            auto_now_add=True,
        )

        def __str__(self):
            return '{}'.format(self.user_name)