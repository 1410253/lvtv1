# Generated by Django 2.2.7 on 2019-11-30 08:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('public', '0012_auto_20191129_1905'),
    ]

    operations = [
        migrations.AddField(
            model_name='apartment',
            name='avatar',
            field=models.TextField(default='', max_length=100),
        ),
    ]
