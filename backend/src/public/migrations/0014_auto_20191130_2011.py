# Generated by Django 2.2.7 on 2019-11-30 20:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('public', '0013_apartment_avatar'),
    ]

    operations = [
        migrations.CreateModel(
            name='CityType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
            ],
        ),
        migrations.DeleteModel(
            name='Article',
        ),
    ]
