# Generated by Django 2.2.7 on 2019-11-29 09:21

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('public', '0005_auto_20191129_1609'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apartment',
            name='bath_rooms',
            field=models.SmallIntegerField(choices=[(0, 0), (1, 1), (2, 2), (3, 3)], default=0),
        ),
        migrations.AlterField(
            model_name='apartment',
            name='bed_rooms',
            field=models.SmallIntegerField(choices=[(0, 0), (1, 1), (2, 2), (3, 3)], default=0),
        ),
        migrations.AlterField(
            model_name='apartment',
            name='dinning_rooms',
            field=models.SmallIntegerField(choices=[(0, 0), (1, 1), (2, 2), (3, 3)], default=0),
        ),
        migrations.AlterField(
            model_name='apartment',
            name='kitchen',
            field=models.SmallIntegerField(choices=[(0, 0), (1, 1), (2, 2), (3, 3)], default=0),
        ),
        migrations.AlterField(
            model_name='apartment',
            name='living_rooms',
            field=models.SmallIntegerField(choices=[(0, 0), (1, 1), (2, 2), (3, 3)], default=0),
        ),
        migrations.AlterField(
            model_name='apartment',
            name='status',
            field=models.SmallIntegerField(),
        ),
        migrations.AlterField(
            model_name='apartment',
            name='toilets',
            field=models.SmallIntegerField(choices=[(0, 0), (1, 1), (2, 2), (3, 3)], default=0),
        ),
    ]
