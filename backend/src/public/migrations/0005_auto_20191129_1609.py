# Generated by Django 2.2.7 on 2019-11-29 09:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('public', '0004_apartmentservices'),
    ]

    operations = [
        migrations.AlterField(
            model_name='apartment',
            name='status',
            field=models.IntegerField(),
        ),
    ]
