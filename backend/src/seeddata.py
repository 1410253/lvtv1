import json
from random import randint
import random
from datetime import datetime
import os

data = []
time = datetime.now()


for i in range(1,20):
    data.append({
        'model': 'public.apartment',
        "pk" : i,
        "fields":{
            "avatar" : 'seed1.jpg',
            "images" : '["seed1.jpg"]',
            "title" : "Apartment Seeded " + str(i),
            "price" : randint(10000000,1000000000),
            "apartment_type" : randint(1,2),
            "status" : randint(0,2),
            "city_id" : randint(0,62),
            "district_id" : randint(0,500),
            "address" : "dia chi seeded",
            "living_rooms" : randint(0,3),
            "dinning_rooms" : randint(0,3),
            "bed_rooms" : randint(0,3),
            "bath_rooms" : randint(0,3),
            "toilets" : randint(0,3),
            "kitchen" : randint(0,3),
            "description" : "description seeded",
            "created_at" : time.strftime("%Y-%m-%d %H:%M"),
            "updated_at" : time.strftime("%Y-%m-%d %H:%M"),
        }
    })

tinh= []

with open('data.json',encoding='utf-8') as f:
    tinh = json.load(f)

index = 1
index1 = 1
for index,value in tinh.items():
    data.append({
        'model': 'public.city',
        "pk" : index,
        "fields":{
            "name" : value["name"]
        }
    })
    for key,value in value["districts"].items():
        data.append({
            'model': 'public.district',
            "pk" : index1,
            "fields":{
                "name" : value,
                "city_id" : index
            }
        })
        index1 += 1
with open('seed.json', 'w') as outfile:
    json.dump(data, outfile)