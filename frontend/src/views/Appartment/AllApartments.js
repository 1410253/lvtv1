import React, { Component } from 'react';
import { Container, Row, Col } from 'react-grid-system';
import 'semantic-ui-css/semantic.min.css'
import { Checkbox } from 'semantic-ui-react'
import avatar from './../../assets/img/avatar.jpg'
import { Icon } from 'semantic-ui-react'
import { Button } from 'semantic-ui-react'
import axios from 'axios'

class AllApartment extends Component {
  state = {
    all_apartment : []
  }

  componentDidMount(){
    axios.get('http://127.0.0.1:8000/api/allapartment')
      .then( res => {
          this.setState({
            all_apartment : res.data
          })
          console.log(this.state .all_apartment)
      })
  }
    render() {
        return (
            <div className="fullbody">
                <Row className="fix">
                <Button.Group className="group-but">
    <Button className="white-button">All</Button>
    <Button>Draft</Button>
    <Button>Active</Button>
  </Button.Group>
  <Button.Group className="group-but1">
    <Button className="white-button"><Icon disabled name='list'className="large"/></Button>
    <Button><Icon disabled name='th list'className="large"/></Button>

  </Button.Group>
                </Row>
                <Row className="fix ">

                <div className="header-all">
                <Checkbox className="header-checkbox"/>
                <Icon disabled name='image'className="header-icon large"/>

                </div>
                <p className="header-title">Title</p>
                <p className="header-area">Area</p>
                <p className="header-date">Last modified</p>
                </Row>
                <Row className="fix">
                    <div className="body-all">
                      {this.state.all_apartment.map(item => (
                        <Row className="fix">
                        <Checkbox className="check-box"/>
                        <div className="img-container">
                          <img src={"images/" + item.avatar} className="all-img"/>
                        </div>
                        <p className="all-title">{item.title}</p>
                        <p className="all-area">Khu A</p>
                        <p className="all-date">{item.updated_at}</p>
                        <div className="all-buton">
                          <Button className="green" data-id={item.id} >Edit
                            <Icon disabled name='pencil' className="icon-but" />
                          </Button>
                          <Icon disabled name='ellipsis vertical' className="all-icon" />
                        </div>
                      </Row>
                      ))}
                    </div>
                </Row>
            </div>
        );
    }
}

export default AllApartment;
