import React, { Component } from 'react';
import { HashRouter, Route, Switch } from 'react-router-dom';
import DetailApartment from './views/DetailApartment';
// import { renderRoutes } from 'react-router-config';
import './App.scss';
import axios from 'axios'

const loading = () => <div className="animated fadeIn pt-3 text-center">Loading...</div>;

// Containers
const DefaultLayout = React.lazy(() => import('./containers/DefaultLayout'));

// Pages
const Login = React.lazy(() => import('./views/Pages/Login'));
const Register = React.lazy(() => import('./views/Pages/Register'));
const Page404 = React.lazy(() => import('./views/Pages/Page404'));
const Page500 = React.lazy(() => import('./views/Pages/Page500'));
require('dotenv').config();
class App extends Component {

  state = {
    articles : []
  }

  componentDidMount(){
    axios.get('http://127.0.0.1:8000/api/')
      .then( res => {
          this.setState({
            articles : res.data
          })
          console.log(res.data)
      })
  }

  render() {
    return (
      // <DetailApartment/>
      <HashRouter>
          <React.Suspense fallback={loading()}>
            <Switch>
              <Route exact path="/login" name="Login Page" render={props => <Login {...props}/>} />
              <Route exact path="/register" name="Register Page" render={props => <Register {...props}/>} />
              <Route exact path="/404" name="Page 404" render={props => <Page404 {...props}/>} />
              <Route exact path="/500" name="Page 500" render={props => <Page500 {...props}/>} />
              <Route path="/" name="Home" render={props => <DefaultLayout {...props}/>} />
            </Switch>
          </React.Suspense>
      </HashRouter>
    );
  }
}

export default App;
